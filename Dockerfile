FROM openjdk:8u262-slim

COPY /tchallenge-service.jar /

CMD ["java", "-jar", "/tchallenge-service.jar"]